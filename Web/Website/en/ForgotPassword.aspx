﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_ForgotPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
<!--Window Dressing Start-->
 <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="../en/Organizations/Original/Styles/main.css">
  <!--Window Dressing End-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>myVRM</title>
    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        if (path == "")
            path = "Organizations/Org_11/CSS/Mirror/Styles/main.css";
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>

    <script language="JavaScript" src="inc/functions.js" type="text/javascript"></script>

    <script type="text/javascript">
        function fnLoginPage() {
            window.location.replace('genlogin.aspx');
            return false;
        }
    </script>
</head>
<body>
    <center>
        <form id="frmV2EmailLogin" autocomplete="off" runat="server">
            <table>
                <tr style="height: 80px">
                    <td>
                    </td>
                </tr>
            </table>
            <table cellpadding="2" cellspacing="2" width="700">
                <tr align="left">
                    <td>
                        <div>
                            <table style="width: 90%">
                                <tr>
                                    <td align="center">
                                        <h3>
                                            Password Assistance
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 1168px">
                                        <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="2" cellspacing="2" width="700">
                                <tr>
                                    <td style="width: 80px">
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:Label ID="LblMessage" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td align="left" colspan="2">
                                        <%--Window Dressing--%>
                                        <b><span class="subtitleblueblodtext">Enter the email address associated with your myVRM account:</span>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td align="right" style="width: 50px">
                                        <%--Window Dressing--%>
                                        <label class="blackblodtext">
                                            Email</label>
                                    </td>
                                    <td align="left" style="width: 580px">
                                        <asp:TextBox ID="TxtEmail" runat="server" CssClass="altText" Style="width: 200px;"
                                            MaxLength="256" autocomplete="off"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 40px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="2">
                                        <table width="80%">
                                            <tr><td align="center">
                                                    <button type="reset" name="Reset" class="altMedium0BlueButtonFormat" style="width:180px;">Reset</button><br />
                                                </td>
                                                <td align="center">
                                                    <button class="altMedium0BlueButtonFormat" style="width:200px" type="button" ID="BtnSubmit" onclick="javascript:if(!checkemail(document.getElementById('TxtEmail').value)) {alert('Invalid Email Address.');document.getElementById('TxtEmail').focus();return false;}"
                                                        runat="server">Submit</button><br />
                                                </td><%--ZD 100420 End--%>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
<br /><br /><br /><br /><br /><br />
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--FB 2500--%>
<%--<!-- #INCLUDE FILE="inc/mainbottom2.aspx" -->--%>
